package simpledb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TableStats represents statistics (e.g., histograms) about base tables in a
 * query. 
 * 
 * This class is not needed in implementing lab1, lab2 and lab3.
 */
public class TableStats {

    private static final ConcurrentHashMap<String, TableStats> statsMap = new ConcurrentHashMap<String, TableStats>();

    static final int IOCOSTPERPAGE = 1000;

    public static TableStats getTableStats(String tablename) {
        return statsMap.get(tablename);
    }

    public static void setTableStats(String tablename, TableStats stats) {
        statsMap.put(tablename, stats);
    }
    
    public static void setStatsMap(HashMap<String,TableStats> s)
    {
        try {
            java.lang.reflect.Field statsMapF = TableStats.class.getDeclaredField("statsMap");
            statsMapF.setAccessible(true);
            statsMapF.set(null, s);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static Map<String, TableStats> getStatsMap() {
        return statsMap;
    }

    public static void computeStatistics() {
        Iterator<Integer> tableIt = Database.getCatalog().tableIdIterator();

        System.out.println("Computing table stats.");
        while (tableIt.hasNext()) {
            int tableid = tableIt.next();
            TableStats s = new TableStats(tableid, IOCOSTPERPAGE);
            setTableStats(Database.getCatalog().getTableName(tableid), s);
        }
        System.out.println("Done.");
    }

    /**
     * Number of bins for the histogram. Feel free to increase this value over
     * 100, though our tests assume that you have at least 100 bins in your
     * histograms.
     */
    static final int NUM_HIST_BINS = 100;

    private int tableid;
    private int ioCostPerPage;
    private DbFile file;
    private int n_fields;
    private int n_tuples;
    private TupleDesc desc;

    private ArrayList<IntHistogram> int_hist = new ArrayList<>();
    private ArrayList<StringHistogram> str_hist = new ArrayList<>();
    private HashMap<Integer, Object> col2hist = new HashMap<>();

    /**
     * Create a new TableStats object, that keeps track of statistics on each
     * column of a table
     * 
     * @param tableid
     *            The table over which to compute statistics
     * @param ioCostPerPage
     *            The cost per page of IO. This doesn't differentiate between
     *            sequential-scan IO and disk seeks.
     */
    public TableStats(int tableid, int ioCostPerPage) {
        // For this function, you'll have to get the
        // DbFile for the table in question,
        // then scan through its tuples and calculate
        // the values that you need.
        // You should try to do this reasonably efficiently, but you don't
        // necessarily have to (for example) do everything
        // in a single scan of the table.
        // some code goes here

        DbFile file = Database.getCatalog().getDatabaseFile(tableid);


        // scan to find max and min for all columns
        desc = file.getTupleDesc();
        n_fields = desc.numFields();

        int [] maxs = new int[n_fields];
        int [] mins = new int[n_fields];

        for (int i = 0; i < n_fields; i++) {
            maxs[i] = Integer.MIN_VALUE;
            mins[i] = Integer.MAX_VALUE;
        }

        DbFileIterator iter = file.iterator(new TransactionId());
        try {
            iter.open();
            while (iter.hasNext()) {
                Tuple tp = iter.next();
                for (int i = 0; i < n_fields; i++) {
                    if (desc.getFieldType(i) == Type.INT_TYPE) {
                        int value =  ((IntField)tp.getField(i)).getValue();
                        maxs[i] = Integer.max(maxs[i], value);
                        mins[i] = Integer.min(mins[i], value);
                    }
                }
            }
        } catch (TransactionAbortedException e) {
            e.printStackTrace();
        } catch (DbException e) {
            e.printStackTrace();
        }

        int n_buckets = 32;

        // build histogram
        for (int i = 0; i < n_fields; i++) {
            switch (desc.getFieldType(i)) {
                case STRING_TYPE:
                    col2hist.put(i, new StringHistogram(n_buckets));
                    break;
                case INT_TYPE:
                    col2hist.put(i, new IntHistogram(n_buckets, mins[i], maxs[i]));
            }
        }
        iter.close();

        // scan to build histogram for all columns
        n_tuples = 0;
        iter = file.iterator(new TransactionId());
        try {
            iter.open();
            while (iter.hasNext()) {
                Tuple tp = iter.next();
                for (int i = 0; i < n_fields; i++) {
                    switch (desc.getFieldType(i)) {
                        case STRING_TYPE: {
                            String value = ((StringField)tp.getField(i)).getValue();
                            ((StringHistogram)col2hist.get(i)).addValue(value);
                        }
                        case INT_TYPE: {
                            int value = ((IntField)tp.getField(i)).getValue();
                            ((IntHistogram)col2hist.get(i)).addValue(value);
                        }
                    }
                }
                n_tuples++;
            }
        } catch (TransactionAbortedException e) {
            e.printStackTrace();
        } catch (DbException e) {
            e.printStackTrace();
        }

        this.tableid = tableid;
        this.ioCostPerPage =ioCostPerPage;
        this.file = file;
    }

    /**
     * Estimates the cost of sequentially scanning the file, given that the cost
     * to read a page is costPerPageIO. You can assume that there are no seeks
     * and that no pages are in the buffer pool.
     * 
     * Also, assume that your hard drive can only read entire pages at once, so
     * if the last page of the table only has one tuple on it, it's just as
     * expensive to read as a full page. (Most real hard drives can't
     * efficiently address regions smaller than a page at a time.)
     * 
     * @return The estimated cost of scanning the table.
     */
    public double estimateScanCost() {
        // some code goes here
        int num_pages;
        if (file instanceof HeapFile) {
            num_pages = ((HeapFile)file).numPages();
        } else if (file instanceof BTreeFile) {
            num_pages = ((BTreeFile)file).numPages();
        } else {
            System.err.println("Unknown file type " + file);
            num_pages = -1;
            assert(false);
        }
        return ioCostPerPage * num_pages;
    }

    /**
     * This method returns the number of tuples in the relation, given that a
     * predicate with selectivity selectivityFactor is applied.
     * 
     * @param selectivityFactor
     *            The selectivity of any predicates over the table
     * @return The estimated cardinality of the scan with the specified
     *         selectivityFactor
     */
    public int estimateTableCardinality(double selectivityFactor) {
        // some code goes here
        return (int)(n_tuples * selectivityFactor);
    }

    /**
     * The average selectivity of the field under op.
     * @param field
     *        the index of the field
     * @param op
     *        the operator in the predicate
     * The semantic of the method is that, given the table, and then given a
     * tuple, of which we do not know the value of the field, return the
     * expected selectivity. You may estimate this value from the histograms.
     * */
    public double avgSelectivity(int field, Predicate.Op op) {
        // some code goes here
        switch (desc.getFieldType(field)) {
            case INT_TYPE:
                return ((IntHistogram)col2hist.get(field)).avgSelectivity();
            case STRING_TYPE:
                return ((StringHistogram)col2hist.get(field)).avgSelectivity();
        }
        return 1.0;
    }

    /**
     * Estimate the selectivity of predicate <tt>field op constant</tt> on the
     * table.
     * 
     * @param field
     *            The field over which the predicate ranges
     * @param op
     *            The logical operation in the predicate
     * @param constant
     *            The value against which the field is compared
     * @return The estimated selectivity (fraction of tuples that satisfy) the
     *         predicate
     */
    public double estimateSelectivity(int field, Predicate.Op op, Field constant) {
        // some code goes here
        switch (desc.getFieldType(field)) {
            case INT_TYPE:
                return ((IntHistogram)col2hist.get(field)).estimateSelectivity(
                        op, ((IntField)constant).getValue());
            case STRING_TYPE:
                return ((StringHistogram)col2hist.get(field)).estimateSelectivity(
                        op, ((StringField)constant).getValue());
        }
        return 1.0;
    }

    /**
     * return the total number of tuples in this table
     * */
    public int totalTuples() {
        // some code goes here
        return n_tuples;
    }

}

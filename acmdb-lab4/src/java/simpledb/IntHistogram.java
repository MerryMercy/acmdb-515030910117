package simpledb;

import static java.lang.Integer.min;
import static java.lang.Integer.max;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {

    /**
     * Create a new IntHistogram.
     * 
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     * 
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     * 
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't 
     * simply store every value that you see in a sorted list.
     * 
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */

    private int [] count;
    private int width;
    private int max, min;
    private int total_ct;

    public IntHistogram(int buckets, int min, int max) {
    	// some code goes here
        count = new int[buckets];

        width = (max - min + 1 + buckets - 1) / buckets;
        total_ct = 0;

        this.min = min;
        this.max = max;
    }

    /**
     * Add a value to the set of values that you are keeping a histogram of.
     * @param v Value to add to the histogram
     */
    public void addValue(int v) {
    	// some code goes here
        assert(v >= this.min && v <= this.max);

        count[(v - this.min) / width]++;
        total_ct++;
    }

    /**
     * Estimate the selectivity of a particular predicate and operand on this table.
     * 
     * For example, if "op" is "GREATER_THAN" and "v" is 5, 
     * return your estimate of the fraction of elements that are greater than 5.
     * 
     * @param op Operator
     * @param v Value
     * @return Predicted selectivity of this particular operator and value
     */
    public double estimateSelectivity(Predicate.Op op, int v) {
    	// some code goes here
        switch (op) {
            case EQUALS:
                if (v < this.min || v > this.max)
                    return 0.0;

                return (float)count[(v - this.min) / width] / total_ct;
            case NOT_EQUALS:
                if (v < this.min || v > this.max)
                    return 1.0;
                return 1 - (float)count[(v - this.min) / width] / total_ct;
            case GREATER_THAN:
            case GREATER_THAN_OR_EQ: {
                int add_one = (op == Predicate.Op.GREATER_THAN_OR_EQ ? 1 : 0);
                if (v < this.min) {
                    v = this.min;
                    add_one = 1;
                }
                if (v > this.max)
                    return 0;

                int bn = (v - this.min) / width;

                float nums = ((bn+1) * width - (v - this.min - add_one)) * (float)count[bn] / width;
                bn += 1;
                while (bn < count.length) {
                    nums += count[bn];
                    bn++;
                }

                return Double.min(nums / total_ct, 1.0);
            }
            case LESS_THAN:
            case LESS_THAN_OR_EQ: {
                int add_one = (op == Predicate.Op.LESS_THAN_OR_EQ ? 1 : 0);
                if (v > this.max) {
                    add_one = 1;
                    v = this.max;
                }
                if (v < this.min)
                    return 0;

                int bn = (v - this.min) / width;

                float nums = ((v - this.min + add_one) - bn * width) * (float)count[bn] / width;
                bn -= 1;
                while (bn >= 0) {
                    nums += count[bn];
                    bn--;
                }

                return Double.min(nums / total_ct, 1.0);
            }
            default:
                System.err.println("Not implemented");
                assert(false);
        }
        return -1.0;
    }
    
    /**
     * @return
     *     the average selectivity of this histogram.
     *     
     *     This is not an indispensable method to implement the basic
     *     join optimization. It may be needed if you want to
     *     implement a more efficient optimization
     * */
    public double avgSelectivity()
    {
        // some code goes here
        return total_ct / (max - min + 1);
    }
    
    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        // some code goes here
        return "buckets: " + count.length + " max: " + max + ", min:" + min + ", width: " + width;
    }
}

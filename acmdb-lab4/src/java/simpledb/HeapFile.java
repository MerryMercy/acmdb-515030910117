package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {
    private File f;
    private TupleDesc td;
    private int id;
    private int numPages;

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
        this.f = f;
        this.td = td;

        id = f.getAbsoluteFile().hashCode();
        numPages = (int)(Math.ceil(f.length() / BufferPool.getPageSize()) + 0.5);
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        return f;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return id;
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        return td;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        int no = pid.pageNumber();
        int offset = no * BufferPool.getPageSize();

        try {
            RandomAccessFile raf = new RandomAccessFile(f, "r");
            byte [] data = new byte[BufferPool.getPageSize()];
            raf.seek(offset);
            raf.read(data);
            HeapPage page = new HeapPage(new HeapPageId(id, no), data);
            raf.close();

            return page;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.print("Cannot find from file");
            return null;
        }
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
        int no = page.getId().pageNumber();
        int offset = no * BufferPool.getPageSize();

        RandomAccessFile raf = new RandomAccessFile(f, "rw");
        raf.seek(offset);
        raf.write(page.getPageData());
        raf.close();

        numPages = (int)(Math.ceil(f.length() / BufferPool.getPageSize()) + 0.5);
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        numPages = (int)(Math.ceil(f.length() / BufferPool.getPageSize()) + 0.5);
        return numPages;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
        HeapPage page = null;

        for (int i = 0; i < numPages; i++) {
            HeapPageId pid = new HeapPageId(id, i);
            page = (HeapPage) Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
            if (page.getNumEmptySlots() > 0)
                break;
        }


        // no empty slots, insert a new page
        if (page == null || page.getNumEmptySlots() <= 0) {
            HeapPageId pid = new HeapPageId(id, numPages++);
            page = new HeapPage(pid, HeapPage.createEmptyPageData());
            writePage(page);
            page = (HeapPage) Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
        }

        page.insertTuple(t);

        ArrayList<Page> ret = new ArrayList<>();
        ret.add(page);
        return ret;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
        HeapPage page;

        page = (HeapPage) Database.getBufferPool().getPage(tid, t.getRecordId().getPageId(), Permissions.READ_WRITE);
        page.deleteTuple(t);

        ArrayList<Page> ret = new ArrayList<>();
        ret.add(page);
        return ret;
    }


    class HeadFileIterator extends AbstractDbFileIterator {
        private HeapFile file;
        private TransactionId tid;
        private int page_ct;
        private Iterator<Tuple> tuple_iter;

        public HeadFileIterator(TransactionId tid, HeapFile file) {
            this.file = file;
            this.tid = tid;
        }

        public void open()
                throws DbException, TransactionAbortedException {
            page_ct = 0;
            HeapPage page = (HeapPage) Database.getBufferPool().getPage(
                    tid, new HeapPageId(file.getId(), page_ct), Permissions.READ_WRITE);
            tuple_iter = page.iterator();
        }

        protected Tuple readNext() throws DbException, TransactionAbortedException {
            if (tuple_iter != null && tuple_iter.hasNext()) {
                return tuple_iter.next();
            } else {
                while (true) {
                    page_ct++;
                    if (page_ct >= file.numPages())
                        return null;
                    HeapPage page = (HeapPage) Database.getBufferPool().getPage(
                            tid, new HeapPageId(file.getId(), page_ct), Permissions.READ_WRITE);
                    tuple_iter = page.iterator();
                    if (tuple_iter.hasNext())
                        break;
                }
                return tuple_iter.next();
            }
        }

        public void rewind() throws DbException, TransactionAbortedException {
            page_ct = -1;
            tuple_iter = null;
        }

        public void close() {
            super.close();
            page_ct = file.numPages();
            tuple_iter = null;
        }
    }


    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
        return new HeadFileIterator(tid, this);
    }

}


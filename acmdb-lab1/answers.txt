# Design decision
It seems there is no design desicision we can made in Lab1.

# API Change
The default APIs just work well for lab1. I did not change them.

# Missing Part
I passed all the test, so there is no missing elements in my code for lab1.

# How long on the lab

I spent about 5 hours on this lab.
There are mainly two things that confused me.

* Java File I/O API
    I use RandomAccessFile class in java to read file. But the usage of raf.read function is conflict with my assumption.
    It tooks me a while to debug.
    Therefore, more documents on the File I/O API of java will be fine.

* Use Database as Global Variable
    Database is a global variable and we should get all information from this global variable (e.g. Database.getCatalog()).
    In the beginning, I was not familiar with this kind of design and wanted to change the API. Later I realized we should query information from this global variable.

